from app import db,login
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

QuestionTag = db.Table('QuestionTag',
    db.Column('qus_id', db.Integer, db.ForeignKey('question.id')),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'))
)

class User(UserMixin,db.Model):
    id = db.Column(db.Integer,primary_key=True)
    username=db.Column(db.Integer, unique=True)
    questions = db.relationship('Question', backref='asker', lazy='dynamic')
    password_hash = db.Column(db.String(128))
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

class Question(db.Model):
    id = db.Column(db.Integer,primary_key=True)
    title=db.Column(db.String(64))
    description=db.Column(db.String(120))
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))
    tags=db.relationship('Tag',secondary=QuestionTag,backref='ques',lazy='dynamic')

class Answer(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    body=db.Column(db.String(250))
    qus_id=db.Column(db.Integer,db.ForeignKey('question.id'))
    user_id=db.Column(db.Integer,db.ForeignKey('user.id'))

class Tag(db.Model):
    id=db.Column(db.Integer,primary_key=True)
    name=db.Column(db.String(64),unique=True)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))
