from flask import render_template, redirect, url_for, request
from app import app,db
from app.models import Question,Answer,Tag,User
from app.forms import NewQusForm,LoginForm,RegistrationForm,AnswerForm
from flask_login import login_user, logout_user, current_user, login_required

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')

@app.route('/answer/<question_id>',methods=['GET','POST'])
def answer_question(question_id):
    form=AnswerForm()
    if request.method=='POST':
        answer=Answer(body=form.ans.data,qus_id=question_id,user_id=current_user.id)
        db.session.add(answer)
        db.session.commit()
        return redirect(url_for('question_detail',question_id=question_id))
    return render_template('ans.html',form=form)


@app.route('/questions/<question_id>',methods=['GET','POST'])
def question_detail(question_id):
    question=Question.query.filter_by(id=question_id).first()
    answers=Answer.query.filter_by(qus_id=question_id).all()
    return render_template('single_qus.html',question=question,answers=answers)

@app.route('/questions',methods=['GET','POST'])
def question_list():
    questions=Question.query.all()
    return render_template('all_qus.html',questions=questions)

@app.route('/new_qus',methods=['GET','POST'])
@login_required
def new_qus():
    form=NewQusForm()
    if request.method=='POST':
        tags=form.tags.data.split(',')
        qus = Question(title=form.title.data,description=form.description.data,user_id=current_user.id)
        db.session.add(qus)
        db.session.commit()
        for t in tags:
            tag=Tag(name=t)
            tag.ques.append(qus)
            db.session.add(tag)
            db.session.commit()
        return redirect(url_for('index'))

    return render_template('ask_qus.html',form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():

    form = LoginForm()
    if request.method=="POST":
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('index'))
    return render_template('login.html', form=form)

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        login_user(user)
        return redirect(url_for('index'))
    return render_template('register.html', title='Register', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
